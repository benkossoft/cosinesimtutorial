import csv
import dictionary
import neo4jdriver


# create movies
def create_movies():
    movies = []
    with open(dictionary.movie_csv) as data:
        csvr = csv.DictReader(data, delimiter=',', quotechar='"')
        for row in csvr:
            movie_id = row["movieId"]
            title = row["title"]
            genres = row["genres"]
            movies.append({"movie_id": int(movie_id), "title": title, "genres": genres})

    with neo4jdriver.session.begin_transaction() as tx:
        tx.run(dictionary.movie_query_create, data=movies)
        tx.commit()


# create users, edges
def create_users_and_ratings():
    ratings = []
    with open(dictionary.rating_csv) as data:
        csvr = csv.DictReader(data, delimiter=',', quotechar='"')
        for row in csvr:
            ratings.append({'user_id': int(row["userId"]),
                            'movie_id': int(row["movieId"]),
                            'rating': float(row["rating"]),
                            'timestamp': row["timestamp"]})

    with neo4jdriver.session.begin_transaction() as tx:
        tx.run(dictionary.rating_query_create, data=ratings)
        tx.commit()


# create similarities
def create_cosine_similarity():
    with neo4jdriver.session.begin_transaction() as tx:
        tx.run(dictionary.cosine_sim_query)
        tx.commit()


# get movie similarities
def get_movie_similarities(movie_id):
    with neo4jdriver.session.begin_transaction() as tx:
        records = tx.run(dictionary.get_sim_of_movie_query, movie_id=movie_id)
        for record in records:
            print movie_id, " - ", record["movie_id"], " = ", record["cosim"]


if __name__ == '__main__':
    # create_movies()
    # create_users_and_ratings()
    # create_cosine_similarity()

    get_movie_similarities(1)
