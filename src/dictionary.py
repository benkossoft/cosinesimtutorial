movie_query_create = "UNWIND {data} as d " \
             "MERGE (m:Movie {movie_id:d.movie_id, title:d.title, genres:d.genres})"
rating_query_create = "UNWIND {data} as d MATCH (m:Movie {movie_id:d.movie_id}) " \
              "MERGE (u:User {user_id:d.user_id}) " \
              "MERGE (u)-[:rates {rating:d.rating, time:d.timestamp}]->(m)"
movie_csv = "../res/movies.csv"
rating_csv = "../res/ratings.csv"
cosine_sim_query = "MATCH (m1:Movie)<-[r1:rates]-(u:User)-[r2:rates]->(m2:Movie) " \
                   "WITH SUM(r1.rating*r2.rating) / (SQRT(SUM(r1.rating^2))*SQRT(SUM(r2.rating^2))) as sim, m1, m2 " \
                   "MERGE (m1)-[s:Cosim {similarity:sim}]-(m2)"
get_sim_of_movie_query = "MATCH (m1:Movie)-[c:Cosim]-(m2:Movie) " \
                         "WHERE m1.movie_id = {movie_id} " \
                         "RETURN m2.movie_id as movie_id, c.similarity as cosim"
